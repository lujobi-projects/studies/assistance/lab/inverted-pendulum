"""Draws the inverted pendulum according to its state vector"""
import numpy as np
import cv2
from tqdm import tqdm

# Adapted from https://github.com/mpkuse/inverted_pendulum


class Display:
    """Draws the inverted pendulum according to its state vector"""
    def __init__(self, *,
                 color_background=None,
                 color_ground=None,
                 color_font=None,
                 color_marks=None,
                 color_pendulum=None,
                 color_cart=None
                 ):

        # Careful cv2 ha the colors in GBR
        self.color_background = color_background if color_background else [
            225, 225, 225]
        self.color_ground = color_ground if color_ground else [19, 69, 139]
        self.color_font = color_font if color_font else [252, 136, 3]
        self.color_marks = color_marks if color_marks else [0, 255, 0]
        self.color_pendulum = color_pendulum if color_pendulum else [0, 0, 0]
        self.color_cart = color_cart if color_cart else [0, 0, 0]

    # pylint: disable=too-many-locals
    def step(self, state_vec, t=None):
        """ state_vec :
                x0 : position of the cart
                x1 : veclocity of the cart
                x2 : angle of pendulum. In ref frame with x as forward of the
                     cart and y as up. Angile with respect to ground plane
                x3 : angular velocity of the pendulum
        """
        cart_pos = state_vec[0]
        bob_angle = state_vec[2]*180. / np.pi  # degrees
        length_of_pendulu = 110.

        # np.zeros( (512, 512,3), dtype='uint8' )
        image = np.full((512, 512, 3), self.color_background, dtype='uint8')

        # Ground line
        cv2.line(image, (0, 450), (image.shape[1], 450), self.color_ground, 5)

        # Mark ground line
        x_start = -5.
        x_end = 5.
        for x_d in np.linspace(x_start, x_end, 11):
            x = int((x_d - x_start) / (x_end - x_start) * image.shape[0])

            cv2.circle(image, (x, 450), 5, self.color_marks, -1)

            cv2.putText(image, str(x_d), (x-15, 450+15),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)

        # Draw Wheels of the cart
        wheel_1_pos = int((cart_pos - 1.2 - x_start) /
                          (x_end - x_start) * image.shape[0])
        wheel_2_pos = int((cart_pos + 1.2 - x_start) /
                          (x_end - x_start) * image.shape[0])

        cv2.circle(image, (wheel_1_pos, 415), 25, self.color_cart, 6)
        cv2.circle(image, (wheel_2_pos, 415), 25, self.color_cart, 6)
        cv2.circle(image, (wheel_1_pos, 415), 2, self.color_cart, -1)
        cv2.circle(image, (wheel_2_pos, 415), 2, self.color_cart, -1)

        # Cart base
        cart_base_start = int((cart_pos - 2.5 - x_start) /
                              (x_end - x_start) * image.shape[0])
        cart_base_end = int((cart_pos + 2.5 - x_start) /
                            (x_end - x_start) * image.shape[0])

        cv2.line(image, (cart_base_start, 380),
                 (cart_base_end, 380), self.color_cart, 6)

        # Pendulum hinge
        pendulum_hinge_x = int((cart_pos - x_start) /
                               (x_end - x_start) * image.shape[0])
        pendulum_hinge_y = 380
        cv2.circle(image, (pendulum_hinge_x, pendulum_hinge_y),
                   10, self.color_pendulum, -1)

        # Pendulum
        pendulum_bob_x = int(length_of_pendulu *
                             np.cos((bob_angle / 180. + 0.5) * np.pi))
        pendulum_bob_y = int(length_of_pendulu *
                             np.sin((bob_angle / 180. + 0.5) * np.pi))
        cv2.circle(image, (pendulum_hinge_x+pendulum_bob_x,
                           pendulum_hinge_y-pendulum_bob_y), 10, self.color_pendulum, -1)
        cv2.line(image,
                 (pendulum_hinge_x, pendulum_hinge_y),
                 (pendulum_hinge_x + pendulum_bob_x,
                  pendulum_hinge_y-pendulum_bob_y),
                 self.color_pendulum,
                 3)

        # Mark the current angle
        angle_display = bob_angle % 360
        if angle_display > 180:
            angle_display = -360+angle_display
        cv2.putText(image,
                    "theta="+str(np.round(angle_display, 4))+" deg",
                    (pendulum_hinge_x-15, pendulum_hinge_y-15),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    0.8,
                    self.color_font,
                    1)

        # Display on top
        if t is not None:
            cv2.putText(image, "t="+str(np.round(t, 4))+"sec", (15, 15),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)
            cv2.putText(image, "ANG="+str(np.round(bob_angle, 4))+" degrees",
                        (15, 35), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)
            cv2.putText(image, "POS="+str(np.round(cart_pos, 4))+" m", (15, 55),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.8, self.color_font, 1)

        return image

    def steps2artist(self, y, ax):
        """ Creates matplotlib artists for every state

        Arguments:
            y {[type]} -- [matrix with its columns being the state vector]
            ax {[type]} -- [axi]

        Returns:
            [array] -- [array of artists]
        """
        image = []
        for col in tqdm(y, desc='Render-Progress', ncols=100):
            rendered = self.step(col)
            converted = cv2.cvtColor(rendered, cv2.COLOR_BGR2RGB)

            image.append([ax.imshow(converted, animated=True)])
        return image
