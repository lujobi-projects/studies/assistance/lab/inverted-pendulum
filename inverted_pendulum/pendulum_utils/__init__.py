"""
Physical System of a pendulum
"""

from scipy.integrate import solve_ivp
import numpy as np

# pylint: disable=too-few-public-methods


class System:
    """
    Physical System of a pendulum
    """

    def __init__(self, *,
                 M=5,
                 m=1,
                 g=9.81,
                 l=1.25,
                 damp_cart=0.05,
                 damp_pendulum=0.05,
                 f=lambda t: 1
                 ):
        """Definition of an inverted pendulum

        Keyword Arguments:
        M {float} -- [mass of the cart in kg] (default: {5})
        m {float} -- [mass of the pendulum in kg] (default: {1})
        g {float} -- [gravitational constant] (default: {9.81})
        l {float} -- [length of pendulum in meters] (default: {1.25})
        damp_cart {float} -- [damping coefficient of the cart] (default: {0.05})
        damp_pendulum {float} -- [damping coefficient of the pendulum] (default: {0.05})
        f {[function]} -- [function definig the force in newton one argument (t)]
            (default: {lambda t:1})
        """

        self.M = M
        self.m = m
        self.g = g
        self.l = l
        self.damp_cart = damp_cart
        self.damp_pendulum = damp_pendulum
        self.f = f

    @staticmethod
    # pylint: disable=too-many-arguments
    def __dz_dt(t, z, M, m, g, l, damp_cart, damp_pendulum, f):
        _, z2, z3, z4 = z
        dx2_dt2 = (f(t) + m*g*np.sin(z3)*np.cos(z3) - m*l*z4*z4 *
                   np.sin(z3)) / (M+(1-np.cos(z3)*np.cos(z3))*m)
        dz_dt_arr = np.zeros(4)
        dz_dt_arr[0] = z2
        dz_dt_arr[1] = dx2_dt2 - damp_cart*z2
        dz_dt_arr[2] = z4
        dz_dt_arr[3] = (dx2_dt2*np.cos(z3)+g*np.sin(z3))/l - damp_pendulum*z4

        return dz_dt_arr

    def solve_ode(self, time_span, z_0, *, t_eval):
        """solves the exact system ODE

        Arguments:
            time_span {[[t_start, t_end]]} -- [tuple of time start and time end of the sumulation]
            z_0 {[array]} -- [initial state array of size 4]
            t_eval {[np.array]} -- [[optional] times at which the solution of the ODE is returned]

        Returns:
            [np.matix] -- [solution of ODE for all states]
        """
        def wrapper(t, z):
            return self.__dz_dt(t, z, self.M, self.m, self.g,
                                self.l, self.damp_cart, self.damp_pendulum, self.f)
        return solve_ivp(wrapper, time_span, z_0, t_eval=t_eval, dense_output=True, max_step=0.01)
