# Inverted Pendulum proof of concept

Readme incoming

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lujobi%2Finverted-pendulum/master?filepath=Pendulum.ipynb)
[![pipeline status](https://gitlab.com/lujobi/inverted-pendulum/badges/master/pipeline.svg)](https://gitlab.com/lujobi/inverted-pendulum/-/commits/master)
[![pylint](https://gitlab.com/lujobi/inverted-pendulum/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint)](https://gitlab.com/lujobi/inverted-pendulum/-/jobs/artifacts/master/browse/pylint/pylint.log?job=pylint)


**use Python 3.x in order to execute**

## Development
Crate a virtal environment and source said venv
``` bash
$ virtualenv venv
$ source venv/bin/activate
```

*If your distro doesn't use python3.x as python (e.g. ubuntu) you need to install python 3 the following way*
``` bash
$ sudo apt install python3
$ virtualenv -p /usr/bin/python3.*X* venv
$ source venv/bin/activate
$ python -V #should display 3.x.x
```

Install dependencies #Todo convert into module
``` bash
$ pip install -r requirements.txt
$ pip install jupyterlab
```
Start jupyter:
``` bash
$ jupyter lab
```